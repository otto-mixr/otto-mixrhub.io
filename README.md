# README #

This is a backup of all of our milestone reports and personal blog for posterity. The original can be viewed [here.](http://otto-mixr.github.io)

The final report can be viewed in the docs folder, or linked directly here.